/*Made by Jere Kaplas and Simo Pöysä
28.11.2015
*/


#include "shell.h"

void sighandler(int sig)
{
	switch (sig) {
		case SIGALRM:
			printf("\nautologout\n");
			exit(0);
		default:
			break;
	}
	return;
}

int main(void)
{
	// Declarations
	char line[MAXLEN], *args[MAXNUM], *args2[MAXNUM], *pipe_args[MAXNUM][MAXNUM], command[MAXLEN], pipe_command[MAXLEN][MAXLEN], **globPtr;
	int background, pid_i, i, outArg, inArg, globresults, pipes[2], fd, n, k;
	FILE *f;
	pid_t pid;
	glob_t globfiles;
	ReadHistory_t History;
	bool input_mode, pipe_mode, output_mode, restartWhile, globFound;

	// One time initializations
	fd = 0;
	n = 0;

	signal(SIGALRM, sighandler);
	signal(SIGINT, sighandler);

	while (1) {
		background = 0;
		outArg = -1;
		inArg = -1;
		restartWhile = false;
		globFound = false;
		
		input_mode = false; // false = No redirection
		output_mode = false;
		pipe_mode = false;

		/* print the prompt */
		char cwd[1024];
		if (getcwd(cwd, sizeof(cwd)) != NULL)
			printf("%s: %s ", getlogin(), cwd);
		else
			perror("getcwd() error");

		printf("> ");

		/* set the timeout for alarm signal (autologout) */
		alarm(LOGOUT); 
		
		/* read the users command */
		if (fgets(line,MAXLEN,stdin) == NULL) {
			printf("\nlogout\n");
			exit(0);
		}
		line[strlen(line) - 1] = '\0';
		
		if (strlen(line) == 0)
			continue;
		
		// prevents writing "!" commands to history	
		if(line[0] != '!'){
			// write line to history
			writeHistory(line);
		}

		/* start to background? */
		if (line[strlen(line)-1] == '&') {
			line[strlen(line)-1] = 0;
			background = 1;
		}

		/* split the command line */
		parseCmd(args, line);	

		if (strlen(args[0]) >= 2 && args[0][0] == '!' && atoi(args[0]+1)) {
			// check if history has n lines
			int x = atoi(args[0]+1);

			History = readHistory(false, x);

			if (x <= History.nlines) {
				// parse history command
				History.str[strlen(History.str) - 1] = '\0';
				parseCmd(args, History.str);
				

			} else {
				printf("Error finding command in history with number: %d\n", x);
			}
		}
	
		if (strcmp(args[0],"exit") == 0) {
			exit(0);
		}  


		// Reset command lines
		command[0] = '\0';
		for (k = 0; k <= n; k++) {
			pipe_command[k][0] = '\0';
		}

		// Parse the user input, and set modes accordingly.
		n = 0;
		for (i = 0; args[i] != NULL; i++) {

			if (*args[i] == '>') {
				output_mode = true; // OUTPUT REDIRECTION 
				outArg = i+1;
			}			
			else if (*args[i] == '<') {
				input_mode = true; // INPUT REDIRECTION
				inArg = i+1;
			}
			else if (*args[i] == '|') {
				pipe_mode = true; // PIPELINE
				parsePipedCmd(pipe_args, pipe_command, n);
				n++;
			}
			else if(strstr(args[i], "*") != NULL) {
			    globresults = glob(args[i], 0, NULL, &globfiles);

				if(globresults == GLOB_NOMATCH){
					printf("Requested file(s) not found with wildcard %s.\n", args[i]);
					restartWhile = true;
					break;
				}
			    else if (globresults == 0){
			        for (globPtr = globfiles.gl_pathv; *globPtr != NULL; ++globPtr){
			                if(*globPtr == NULL) {
			                	break; 
			                }
			                strcat(command, *globPtr);
			                strcat(command, " ");
			                strcat(pipe_command[n], *globPtr);
			                strcat(pipe_command[n], " ");
					
			        }
			        globfree(&globfiles);
			    }
				globFound = true;
			}
			else {
				if (args[i] == args[outArg]) { }
				else if (args[i] == args[inArg]) { }
				else {
					strcat(command, args[i]);
					strcat(command, " ");
					strcat(pipe_command[n], args[i]);
					strcat(pipe_command[n], " ");
				}
			}
		}

		// restarts while loop if files are not found with glob()
		if(restartWhile == true && globFound == false) continue;

		// Send the parsed command lines to be parsed further into tokenized arrays.
		parseCmd(args2, command);
		parsePipedCmd(pipe_args, pipe_command, n);

		// change directory
		if (strcmp(args[0],"cd") == 0) {
			shell_cd(args);
			continue;
		} 
		
		// read history
		if (strcmp(args[0],"history") == 0) {
			readHistory(true, 0);
			continue;
		} 
	
		/* run piped commands */
		if (pipe_mode == true) {
	        i = 0;
	        while (pipe_args[i][0] != NULL) {
		        pipe(pipes);
		        if ((pid = fork()) == -1) 
		        {
		        	perror("fork");
		        } 
		        else if (pid == 0) 
		        {
		            dup2(fd, 0);
		            if (pipe_args[i+1][0] != NULL) {
		            	dup2(pipes[1], 1);
		            }
		            close(pipes[0]);
		            execvp(pipe_args[i][0], pipe_args[i]);
		            exit(1);
		        } 
		        else 
		        {
		            wait(NULL);
		            close(pipes[1]);
		            fd = pipes[0];
		        }

		        i++;
	        }
		}	
		else {
        /* run command (includeds redirection) */
			switch (pid_i = fork()) {
				case -1:
					/* error */
					perror("fork");
					continue;
				case 0:

					if (output_mode == true) {
						f = fopen(args[outArg], "w");
						if (f == NULL) {
							perror("fopen");
							exit(1);
						}
						dup2(fileno(f), fileno(stdout));
						fclose(f);
					}
					if (input_mode == true) {
						f = fopen(args[inArg], "r");
						if (f == NULL) {
							perror("fopen");
							exit(1);
						}
						dup2(fileno(f), fileno(stdin));
						fclose(f);
					}

					/* child process */
					execvp(args2[0], args2);
					perror("execvp");
					exit(1);
				default:
					/* parent (shell) */
					if (!background) {
						alarm(0);
						//waitpid(pid_i, NULL, 0);
						while (wait(NULL)!=pid_i)
							printf("some other child process exited\n");
					}
					break;
			}
		}
	}
	return 0;
}

// change directory
int shell_cd(char **args)
{
	char *homedir = getenv("HOME");

	if (args[1] == NULL) {
		if (chdir(homedir) != 0) {
    		perror("chdir NULL");
    	}
  	} 
  	else {
    	if (chdir(args[1]) != 0) {
    		perror("chdir");
    	}
  	}
  	return 1;
}

void writeHistory(char *line) 
{
	FILE *historyfile = fopen("history.txt", "a");
	if (!historyfile)
	{
	    perror("fopen");
	    exit(1);
	}
	/* write the command line to history */
	fprintf(historyfile, "%s\n", line);

	fclose(historyfile);
}

// print = print lines to shell or not, n = wanted line
ReadHistory_t readHistory(bool print, int n)
{
	char line[MAXLEN];
	int i = 0;
	ReadHistory_t tempHistory;
	FILE *historyfile = fopen("history.txt","r");
	if (!historyfile){
		perror("fopen");
		exit(1);
	}

	while(fgets(line, MAXLEN, historyfile) != NULL) {
		i++;
		if (i == n) {
			strcpy(tempHistory.str, line);
		}
		if (print) {
			printf("%d %s", i, line);
		}
	}
	tempHistory.nlines = i;
	fclose(historyfile);
	return tempHistory;
}

int parseCmd(char **args, char *line) 
{
	char *cmd = line;
	int i = 0;

	while ((args[i] = strtok(cmd, " ")) != NULL) {
		//printf("arg %d: %s\n", i, args[i]);
		i++;
		cmd = NULL;
	}
	return 1;
}

int parsePipedCmd(char *args[][MAXNUM], char line[][MAXLEN], int n) 
{	
	char *cmd = line[n];
	int i = 0;

	while ((args[n][i] = strtok(cmd, " ")) != NULL) {
		//printf("pipe_arg[%d] %d: %s\n", n, i, args[n][i]);
		i++;
		cmd = NULL;
	}
	return 1;
}
