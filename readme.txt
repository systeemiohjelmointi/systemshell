Systemshell, a simple POSIX compliant Unix shell.
Created by Jere Kaplas (0403105) and Sim Pöysä (0403215)
For Systeemiohjelointi (BM40A0400) course at LUT.


Bonus features implemented:
1. Expanding wildcards with glob()
2. Being able to cope with arbitrary number of pipes

About features:
- History excludes !x commands from the history, to prevent looping the !x commands themselves.
- Reading history has a struct defined specifically for it, since we want to return two values while reading from the history file (specific line, and number of lines total).
- The header also implements typedef for bools, to make the code a little easier to read.
- User input is parsed into varíables, which get further parsed by strtok, which is what gets finally executed.
- Changing directory includes special case for "cd", to change directory to the users home dir.
- Executing commands is seperated to piped and non-piped commands, non-piped supports redirection and background execution.