#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>
#include <glob.h>
#include <fcntl.h>

typedef enum { false, true } bool;

#define LOGOUT 999
#define MAXNUM 40
#define MAXLEN 160

typedef struct {
	int nlines;
	char str[MAXLEN];
} ReadHistory_t;

int shell_cd(char **args);
void writeHistory(char *line);
void sighandler(int sig);	
void writeHistory(char *line);
ReadHistory_t readHistory(bool print, int n);
int parseCmd(char **args, char *line);
int parsePipedCmd(char *args[][MAXNUM], char line[][MAXLEN], int n);